#!/bin/bash

set -xeuo pipefail

cd /jsass/src/main

rm -fr resources/darwin-aarch64
mkdir -p resources/darwin-aarch64

# *** Build libsass

make -C libsass clean
cd libsass
git reset --hard # hard reset
git clean -xdf # hard clean
cd ..

# We use:
# CC=aarch64-apple-darwin22.2-clang            cross compile with osxcross C compiler
# CXX=aarch64-apple-darwin22.2-clang++-libc++  cross compile with osxcross C++ compiler
# WINDRES=aarch64-apple-darwin22.2-ar          cross compile with osxcross
# BUILD="static"                            to make sure that we build a static library
CFLAGS="-Wall -arch aarch64 -stdlib=libc++" \
CXXFLAGS="-Wall -arch aarch64 -stdlib=libc++" \
LDFLAGS="-stdlib=libc++" \
CC=/osxcross/bin/aarch64-apple-darwin22.2-clang \
CXX=/osxcross/bin/aarch64-apple-darwin22.2-clang++-libc++ \
AR=/osxcross/bin/aarch64-apple-darwin22.2-ar \
BUILD=static \
    make -C libsass -j4

# *** Build libjsass

rm -fr c/build
mkdir -p c/build
cd c/build
cmake -DCMAKE_TOOLCHAIN_FILE=/jsass/src/main/c/Toolchain-darwin-aarch64.cmake ../
make
cp libjsass.dylib ../../resources/darwin-aarch64/libjsass.dylib

# *** Build libjsass_test

cd /jsass/src/test

rm -fr resources/darwin-aarch64
mkdir -p resources/darwin-aarch64

rm -fr c/build
mkdir -p c/build
cd c/build
cmake -DCMAKE_TOOLCHAIN_FILE=/jsass/src/main/c/Toolchain-darwin-aarch64.cmake ../
make
cp libjsass_test.dylib ../../resources/darwin-aarch64/libjsass_test.dylib

#!/bin/bash

set -xeuo pipefail

cd /jsass/src/main

rm -fr resources/darwin-x86_64
mkdir -p resources/darwin-x86_64

# *** Build libsass

make -C libsass clean
cd libsass
git reset --hard # hard reset
git clean -xdf # hard clean
cd ..

# We use:
# CC=x86_64-apple-darwin22.2-clang            cross compile with osxcross C compiler
# CXX=x86_64-apple-darwin22.2-clang++-libc++  cross compile with osxcross C++ compiler
# WINDRES=x86_64-apple-darwin22.2-ar          cross compile with osxcross
# BUILD="static"                            to make sure that we build a static library
CFLAGS="-Wall -arch x86_64 -stdlib=libc++" \
CXXFLAGS="-Wall -arch x86_64 -stdlib=libc++" \
LDFLAGS="-stdlib=libc++" \
CC=/osxcross/bin/x86_64-apple-darwin22.2-clang \
CXX=/osxcross/bin/x86_64-apple-darwin22.2-clang++-libc++ \
AR=/osxcross/bin/x86_64-apple-darwin22.2-ar \
BUILD=static \
    make -C libsass -j4

# *** Build libjsass

rm -fr c/build
mkdir -p c/build
cd c/build
cmake -DCMAKE_TOOLCHAIN_FILE=/jsass/src/main/c/Toolchain-darwin-amd64.cmake ../
make
cp libjsass.dylib ../../resources/darwin-x86_64/libjsass.dylib

# *** Build libjsass_test

cd /jsass/src/test

rm -fr resources/darwin-x86_64
mkdir -p resources/darwin-x86_64

rm -fr c/build
mkdir -p c/build
cd c/build
cmake -DCMAKE_TOOLCHAIN_FILE=/jsass/src/main/c/Toolchain-darwin-amd64.cmake ../
make
cp libjsass_test.dylib ../../resources/darwin-x86_64/libjsass_test.dylib
